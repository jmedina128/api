<?php


	//Autocarga las clases

	spl_autoload_register(function ($className){
		$className = str_replace("\\", DIRECTORY_SEPARATOR, $className);
		include_once $_SERVER["DOCUMENT_ROOT"] . '/api/' . $className . '.php';
	});



	use App\Http\Router;

	$router = new Router;
	$router->addRoute('GET', "user/", "UserController@index");
	$router->addRoute('POST', "user/request/", "UserController@verRequest");
	$router->listen();

 ?>