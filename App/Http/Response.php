<?php 
namespace App\Http;

class Response{

	/*

		convierte array en json y imprime.
		Tambien recibe un parametro $code 
		que es el codigo http 200, 404, 401...

	*/

	public static function Json($data = [], $code=200){
		header("Access-Control-Allow-Origin: *");
		header("Content-Type: application/json");
		http_response_code($code);
		$data = json_encode($data);
		echo $data;
	}
}

 ?>