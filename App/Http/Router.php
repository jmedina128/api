<?php 
namespace App\Http;

use App\Http\Response;

class Router{

	//Aqui se guardaran las rutas
	public $routes = []; 

	//raiz de la url
	public $root = "/api/?/";

	/*
	
		$method @ POST o GET
		$route @ nombre de la ruta
		$controller @ nombre del controllador ej class@method

	*/
	public function addRoute($method, $route, $controller, $protected = "false"){
		array_push($this->routes, [
			"route" => $route,
			"controller" => $controller,
			"method" => $method,
			"protected" => $protected
		]);
	}

	/*

		Esto basicamente lee el url y extrae la info que necesitamos para
		llamar al metodo especifico de cada controlador.

	*/

	public function listen(){
		$uri = $_SERVER["REQUEST_URI"];
		$_uri = str_replace($this->root, "", $uri);
		$route = $this->getRoute($_uri);
		if ($route) {
			$class = explode("@", $route["controller"])[0];
			$method = explode("@", $route["controller"])[1];

			if ($_SERVER["REQUEST_METHOD"] != $route["method"]) return Response::Json(["Error" => "Method not allowed"], 404); 

			call_user_func(array("App\\Controllers\\" . $class, $method));
		}else{
			return Response::Json(["Error" => "Route not found"], 404);
		}
 		 	
	}

	/*

	verifica que el router este en el array de routes y 
	lo devuelve en caso de que lo encuentre.

	*/

	public function getRoute($route){
		
		foreach ($this->routes as $_route) {
			if ($_route["route"] == $route) {
				return $_route;
			}
		}
		return false;
	}

}

 ?>