<?php 
namespace App\Models;

use App\Database\Model;
use App\Http\Response;

class User extends Model{

	protected $table = "users";
	protected $fields = ["nombre", "email", "password", "token"];

	public function register($nombre, $email, $pass){
		$this->nombre = $nombre;
		$this->email = $email;
		$this->password = password_hash($pass, PASSWORD_BCRYPT, [12]);
		$this->token = uniqid();
		return $this->save();
	}

	public function login($email, $pass){
		$_this = $this->select()
		->where('email', '=', $email)
		->get('first');
		if (!$_this) {
			return Response::Json(["message" => "email not found"], 401);
		}
		if(password_verify($pass, $_this->password)){
			return Response::Json($_this, 200);
		}else{
			return Response::Json(["message" => "wrong password"], 401);
		}
	}

	public function index(){
		$_this = $this->select(["id", "nombre", "email"])->get();
		return Response::Json($_this);
	}

}

 ?>