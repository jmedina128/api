<?php 
namespace App\Database;

use App\Database\Connection;

abstract class Model extends Connection{
	
	protected $query;
	protected $fields;
	protected $table;

	public function __construct(){
		parent::__construct();
	}

	public function select($fields = ['*']){
		$this->query = "SELECT ".implode(",", $fields)." FROM $this->table";
		return $this;
	}

	public function where($field, $op, $val){
		
		$_query = explode(" ", $this->query);

		if (in_array("WHERE", $_query)) {
			$this->query .= " AND $field $op '$val' ";
		}else{
			$this->query .= " WHERE $field $op '$val' ";	
		}

		return $this;
	}

	public function save(){
		$values = array_map(function ($v){
			return "'".$this->getProperty($v)."'";
		}, $this->fields);
		$this->query = "INSERT INTO $this->table(". implode(",", $this->fields) .") VALUES (". implode(",", $values) .")";
		$result = $this->exInsert($this->query);
		return $this->select()->where("id", '=', $result)->get();
	}

	public function get($type="all"){
		$data = $this->exQuery($this->query);
		$models = [];
		foreach ($data as $key => $value) {
			$model = new $this;
			foreach ($data[$key] as $_key => $_value) {
				$model->$_key = $_value;
			}
			array_push($models, $model);
		}

		if (sizeof($models) < 1) {
			return false;
		}

		switch ($type) {
			case 'all':
				return $models;
				break;
			case 'first':
				return $models[0];
				break;
			case 'last':
				return end($models);
				break;
			default:
				return $models;
				break;
		}
	}

	public function getProperty($val){
		return (isset($this->$val)) ? $this->$val : 'NULL';
	} 

}


 ?>