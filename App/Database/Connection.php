<?php 
namespace App\Database;

use PDO;

abstract class Connection{

	protected $conn = null;

	public function __construct(){
		extract($this->config);
		try {
			$this->conn = new PDO("$driver:host=$host;dbname=$dbname;", $user, $pass);
			$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		} catch (Exception $e) {
			print_r($e->getMessage());
		}
		return $this->conn;
	}

	protected function exInsert($query){
		$stmt = $this->conn->prepare($query);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute();	
		return $this->conn->lastInsertId("*");				
	}

	protected function exQuery($query){

		$stmt = $this->conn->prepare($query);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute();	
		return $stmt->fetchAll();

	}


	private $config = [

		"driver" => "mysql",
		"host" => "localhost",
		"dbname" => "maraton",
		"user" => "root",
		"pass" => "123456",

	];

}
	

 ?>